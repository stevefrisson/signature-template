// const agent = navigator.platform;
// if (!Boolean(agent)) agent = navigator.userAgent;
// const deviceIs = {
//     mobile: agent.match(/Android; Mobile|opera mini|opera mobi|skyfire|maemo|windows phone|palm|iemobile|symbian|symbianos|fennec/i),
//     iPhone: agent.match(/iPhone/i),
//     iPad: agent.match(/ipad/i),
//     tab: agent.match(/ipad|android 3|gt-p7500|sch-i800|playbook|tablet|kindle|gt-p1000|sgh-t849|shw-m180s|a510|a511|a100|dell streak|silk/i),
//     iPod: agent.match(/iPod/i),
//     BB: agent.match(/BlackBerry/i),
//     webOS: agent.match(/webOS/i)
// };
// function isMobileScreen() {
//     if (deviceIs.iPhone || deviceIs.iPod || deviceIs.BB || deviceIs.webOS || deviceIs.mobile || screen.width <= 480) {
//         return true;
//     }
//     return false;
// }
// alert(isMobileScreen());
const form = document.getElementById("signature-form");
const canvas = document.getElementById("signature-pad");
const clear_button = document.querySelector('.clear-button');
const context = canvas.getContext('2d');
context.lineWidth = 3;
context.lineJoin = context.lineCap = 'round';

let writingMode = false;

clear_button.addEventListener('click', event => {
    event.preventDefault();
    clearPad();
});
const passiveObj = { passive: true };
canvas.addEventListener('pointerup', () => { 
    writingMode = false;
}, passiveObj);
canvas.addEventListener('touchend', () => {
    writingMode = false;
}, passiveObj);
canvas.addEventListener('pointerdown', event => {
    writingMode = true;
    context.beginPath();

    const [positionX, positionY] = getTargetPosition(event);
    context.moveTo(positionX, positionY);
}, passiveObj);
canvas.addEventListener('touchstart', event => {
    writingMode = true;
    context.beginPath();

    const [positionX, positionY] = getTargetPositionTouchDevices(event);
    context.moveTo(positionX, positionY);
}, passiveObj);
canvas.addEventListener('pointermove', event => {
    if (!writingMode) return;
    const [positionX, positionY] = getTargetPosition(event);
    context.lineTo(positionX, positionY);
    context.stroke();
}, passiveObj);
canvas.addEventListener('touchmove', event => {
    if (!writingMode) return;
    const [positionX, positionY] = getTargetPositionTouchDevices(event);
    context.lineTo(positionX, positionY);
    context.stroke();
}, passiveObj);

form.addEventListener('submit', event => {
    event.preventDefault();
    const imageURL = canvas.toDataURL();
    const image = document.createElement('img');
    image.src = imageURL;
    image.height = canvas.height;
    image.width = canvas.width;
    image.style.display = 'block';
    form.appendChild(image);
    clearPad();
});

function clearPad() {
    context.clearRect(0, 0, canvas.width, canvas.height);
}

function getTargetPosition(event){
    positionX = event.clientX - event.target.getBoundingClientRect().x;
    positionY = event.clientY - event.target.getBoundingClientRect().y;

    return [positionX, positionY];
}
function getTargetPositionTouchDevices(event) {
    const target = event.touches[0];
    positionX = target.clientX - event.target.getBoundingClientRect().x;
    positionY = target.clientY - event.target.getBoundingClientRect().y;
    return [positionX, positionY];
}