const express = require('express');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/public/css"));
app.use(express.static(__dirname + "/public/js"));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/views/index.html');
});

app.post('/', (req, res) => {
    console.log(req.body);
    res.redirect('/')
});

const PORT_NUMBER = 3000;
app.listen(
    PORT_NUMBER, () => {
        console.log(`App is running on port number ${PORT_NUMBER}`);
    }
);
